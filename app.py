from flask import Flask, request, jsonify
from flask_cors import CORS
from oss2.models import PartInfo
import oss2
from oss2 import SizedFileAdapter

app = Flask(__name__)


@app.route('/')
def hello_world():  # put application's code here

    # 阿里云账号AccessKey拥有所有API的访问权限，风险很高。强烈建议您创建并使用RAM用户进行API访问或日常运维，请登录RAM控制台创建RAM用户。
    auth = oss2.Auth('AccessKey', 'AccessSE')
    # Endpoint以杭州为例，其它Region请按实际情况填写。
    bucket = oss2.Bucket(auth, 'http://oss-cn-shenzhen.aliyuncs.com', 'weiju-msg')

    # 上传文件到OSS。
    # <yourObjectName>由包含文件后缀，不包含Bucket名称组成的Object完整路径，例如abc/efg/123.jpg。
    # <yourLocalFile>由本地文件路径加文件名包括后缀组成，例如/users/local/myfile.txt。
    bucket.put_object_from_file('input.docx', '/Users/lihui/Downloads/input.docx')

    return 'Hello World!'


@app.route("/download")
def download():
    # 阿里云账号AccessKey拥有所有API的访问权限，风险很高。强烈建议您创建并使用RAM用户进行API访问或日常运维，请登录RAM控制台创建RAM用户。
    auth = oss2.Auth('AccessKey', 'AccessSE')
    # Endpoint以杭州为例，其它Region请按实际情况填写。
    bucket = oss2.Bucket(auth, 'http://oss-cn-shenzhen.aliyuncs.com', 'weiju-msg')

    # 下载OSS文件到本地文件。
    # <yourObjectName>由包含文件后缀，不包含Bucket名称组成的Object完整路径，例如abc/efg/123.jpg。
    # <yourLocalFile>由本地文件路径加文件名包括后缀组成，例如/users/local/myfile.txt。
    bucket.get_object_to_file('input.docx', '/Users/lihui/Downloads/input-2.docx')

    return "download"


@app.route("/del")
def delete():
    # 阿里云账号AccessKey拥有所有API的访问权限，风险很高。强烈建议您创建并使用RAM用户进行API访问或日常运维，请登录RAM控制台创建RAM用户。
    auth = oss2.Auth('AccessKey', 'AccessSE')
    # Endpoint以杭州为例，其它Region请按实际情况填写。
    bucket = oss2.Bucket(auth, 'http://oss-cn-shenzhen.aliyuncs.com', 'weiju-msg')

    bucket.delete_object('input.docx')

    return "delete"


@app.route("/init_multipart_upload", methods=['POST'])
def init_multipart_upload():
    key = request.values.get("key")

    # 阿里云账号AccessKey拥有所有API的访问权限，风险很高。强烈建议您创建并使用RAM用户进行API访问或日常运维，请登录RAM控制台创建RAM用户。
    auth = oss2.Auth('AccessKey', 'AccessSE')
    # Endpoint以杭州为例，其它Region请按实际情况填写。
    bucket = oss2.Bucket(auth, 'http://oss-cn-shenzhen.aliyuncs.com', 'weiju-msg')

    upload_id = bucket.init_multipart_upload(key).upload_id

    return upload_id, 200


@app.route("/upload_part", methods=['POST'])
def upload_part():
    key = request.values.get("key")
    upload_id = request.values.get("uploadId")
    part_number = request.values.get("part_number")
    uploaded_file = request.files['file']

    # 阿里云账号AccessKey拥有所有API的访问权限，风险很高。强烈建议您创建并使用RAM用户进行API访问或日常运维，请登录RAM控制台创建RAM用户。
    auth = oss2.Auth('AccessKey', 'AccessSE')
    # Endpoint以杭州为例，其它Region请按实际情况填写。
    bucket = oss2.Bucket(auth, 'http://oss-cn-shenzhen.aliyuncs.com', 'weiju-msg')

    result = bucket.upload_part(key, upload_id, part_number, uploaded_file)
    print('上传完成', result)

    info = dict()
    info['part_number'] = part_number
    info['etag'] = result.etag

    return jsonify(info), 200


@app.route("/complete_multipart_upload", methods=['POST'])
def complete_multipart_upload():
    key = request.json['key']
    upload_id = request.json['upload_id']
    parts = request.json['parts']

    parts_info_array = []
    for item in parts:
        parts_info_array.append(PartInfo(item["part_number"], item["etag"]))

    # 阿里云账号AccessKey拥有所有API的访问权限，风险很高。强烈建议您创建并使用RAM用户进行API访问或日常运维，请登录RAM控制台创建RAM用户。
    auth = oss2.Auth('AccessKey', 'AccessSe')
    # Endpoint以杭州为例，其它Region请按实际情况填写。
    bucket = oss2.Bucket(auth, 'http://oss-cn-shenzhen.aliyuncs.com', 'weiju-msg')

    # 完成分片上传。
    # 如需在完成分片上传时设置相关Headers，请参考如下示例代码。
    headers = dict()
    # 设置文件访问权限ACL。此处设置为OBJECT_ACL_PRIVATE，表示私有权限。
    headers["x-oss-object-acl"] = oss2.OBJECT_ACL_DEFAULT
    bucket.complete_multipart_upload(key, upload_id, parts_info_array, headers=headers)

    return "合并成功", 200


CORS(app, resources=r'/*')
if __name__ == '__main__':
    app.run()
